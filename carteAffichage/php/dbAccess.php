<?php
try {
    $dns = 'mysql:host=localhost:8889;dbname=gestion';
    $utilisateur = 'root';
    $motDePasse = 'root';

    // Options de connection
    $options = array(
      PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    );

    // Initialisation de la connection
    $connexion = new PDO( $dns, $utilisateur, $motDePasse, $options);
    error_log('DB connectée avec succès');
} 
catch ( Exception $e ) {
    die("Erreur de connection à la BDD. ");
} 