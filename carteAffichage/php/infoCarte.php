<?php
include_once('dbAccess.php');

if (isset($_POST['mode'])) {
  $vars = $_POST;
} else {
  $vars = $_GET;
}
switch ($vars['mode']) {
  case 'getPointsCarto':
    $req = 'SELECT info.*, ia.annee, bo.name_option, bo.option_court, imf.name_mode, imf.iut_mode_court';
    $req .= ' FROM (SELECT DISTINCT id_iut FROM iut_info) ii';
    $req .= ' LEFT JOIN iut_formation_annee ifa ON ifa.id_iut = ii.id_iut';
    $req .= ' LEFT JOIN `iut-annee` ia ON ia.id_annee = ifa.iut_annee';
    $req .= ' LEFT JOIN but_option bo ON bo.id_option = ifa.iut_option';
    $req .= ' LEFT JOIN iut_mode_formation imf ON imf.id_mode = ifa.iut_mode';
    $req .= ' LEFT JOIN iut_info info ON info.id_iut = ii.id_iut ';
    $req .= ' WHERE 1=1';
    if (strlen($vars['option']) > 0) {
      $req .= ' AND bo.option_court ="' . strtoupper($vars['option']) . '"';
    }
    if (strlen($vars['modeFormation']) > 0) {
      $req .= ' AND iut_mode_court ="' . strtoupper($vars['modeFormation']) . '"';
    }
    if ($vars['annee'] > 0) {
      $req .= ' AND id_annee ="' . strtoupper($vars['annee']) . '"';
    }
    error_log($req);
    $select = $connexion->query($req);
    $retour = $select->fetchAll(PDO::FETCH_ASSOC);
    //error_log(json_encode($result));
    //$retour = array_unique(json_encode($result));
    break;
}

echo json_encode($retour);
