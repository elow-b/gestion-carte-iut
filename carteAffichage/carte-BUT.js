const map = L.map('mapBut').setView([46.603354, 1.888334], 6);
var layerGroupMap = L.layerGroup().addTo(map);
let svgBoundsMap = [
	[51.1379, -9.0092],
	[41.2242, 9.2259],
];
initMap(svgBoundsMap, 'carteAffichage/assets/france.svg', map);

const mapR = new L.map('mapR', { zoomControl: false }).setView(
	[-21.130738, 55.53648],
	8
);
var layerGroupMapReunion = L.layerGroup().addTo(mapR);
let svgBoundsMapReunion = [
	[-20.708422, 54.812661],
	[-21.55078, 56.231832],
];
const tileLayerR = L.tileLayer(
	'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
).addTo(mapR);
initMap(svgBoundsMapReunion, 'carteAffichage/assets/reunion.svg', mapR);

const mapIdf = new L.map('mapIdf', { zoomControl: false }).setView(
	[48.831893, 2.402521],
	9
);
var layerGroupMapIleDeFrance = L.layerGroup().addTo(mapIdf);
let svgBoundsMapIdf = [
	[49.18773, 2.076607],
	[48.544522, 2.93646],
];
initMap(svgBoundsMapIdf, 'carteAffichage/assets/ile-de-france.svg', mapIdf);

const mapGuyane = new L.map('mapGuyane', { zoomControl: false }).setView(
	[4.399823, -52.961234],
	6
);
// const tileLayerG = L.tileLayer(
// 	'https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png'
// ).addTo(mapGuyane);
var layerGroupMapGuyane = L.layerGroup().addTo(mapGuyane);
let svgBoundsMapGuyane = [
	[6.07746, -54.968582],
	[2.023091, -51.375488],
];
initMap(svgBoundsMapGuyane, 'carteAffichage/assets/guyane.svg', mapGuyane);

const tabIdIutHorsMap = [13, 10, 25, 28, 29];
const idReunion = 25;
const idGuyane = 13;
let paramSearch = {
	option: 'commun',
	modeFormation: 'FI',
	annee: 1,
};

$(document).ready(function () {
	$('#anneeBut a').click((lien) => {
		paramSearch.annee = $(lien.target).parent().attr('aria-labelledBy');
		paramSearch.modeFormation = $(lien.target).attr('id');
		Object.values($('.badge-info')).forEach((badge) => {
			if ($(badge).attr('id') === paramSearch.annee) {
				if ($(badge).children().hasClass('hide')) {
					$(badge).children().removeClass('hide');
				}
				$(badge).children().text(paramSearch.modeFormation);
			} else {
				if (
					$(badge).children().attr('id') === 'badgeForm' &&
					!$(badge).children().hasClass('hide')
				) {
					$(badge).children().addClass('hide');
				}
			}
		});
		if (parseInt(paramSearch.annee) !== 1) {
			$('.filtreOptions a.opt').removeClass('disabled');
			$('.filtreOptions a.com').addClass('disabled');
		} else {
			$('.filtreOptions a.com').removeClass('disabled');
			$('.filtreOptions a.opt').addClass('disabled');
		}
		getListePoints();
	});
	$('.filtreOptions a').click((lien) => {
		paramSearch.option = $(lien.target).attr('id');
		getListePoints(
			paramSearch.annee,
			paramSearch.modeFormation,
			paramSearch.option
		);
	});
});

function toggleClassActiveOption() {
	Object.values($('a.opt')).forEach((a) => {
		if ($(a).attr('id') === paramSearch.option) {
			$(a).removeClass('badge-success');
			$(a).addClass('badge-warning');
		} else {
			$(a).addClass('badge-success');
			$(a).removeClass('badge-warning');
		}
	});
}

function initMap(svgBounds, svgUrl, mapSelect) {
	fetch(svgUrl)
		.then((response) => response.text())
		.then((svgText) => {
			var parser = new DOMParser();
			var svgElement = parser.parseFromString(
				svgText,
				'image/svg+xml'
			).documentElement;

			var svgOverlay = L.svgOverlay(svgElement, svgBounds, {
				opacity: 1,
				interactive: true,
			}).addTo(mapSelect);

			var paths = svgElement.querySelectorAll('path');
			paths.forEach(function (path) {
				path.addEventListener('mouseover', function () {
					path.style.fill = '#9C9C9A';
				});

				path.addEventListener('mouseleave', function () {
					path.style.stroke = '';
					path.style.fill = '';
				});
			});
		});
}
getListePoints();

function getListePoints() {
	if (parseInt(paramSearch.annee) === 1) {
		paramSearch.option = 'commun';
	}
	if (paramSearch.annee > 1 && paramSearch.option === 'commun') {
		paramSearch.option = 'cyber';
	}
	paramSearch.mode = 'getPointsCarto';

	layerGroupMap.clearLayers();
	layerGroupMapReunion.clearLayers();

	toggleClassActiveOption();
	$.get('carteAffichage/php/infoCarte.php', paramSearch).then((data) => {
		let listePoints = JSON.parse(data);
		if (listePoints.length > 0) {
			createMarker(JSON.parse(data), paramSearch.option);
		}
	});
}

function getIconeSelonOption(option) {
	switch (option) {
		case 'cyber':
			return 'target-cyber.svg';
			break;
		case 'devcloud':
			return 'target-devcloud.svg';
			break;
		case 'iom':
			return 'target-iom.svg';
			break;
		case 'pilpro':
			return 'target-pilpro.svg';
			break;
		case 'rom':
			return 'target-rom.svg';
			break;
		default:
			return 'target.svg';
			break;
	}
}

function createMarker(pointsIUT, option) {
	let iconPerso = L.icon({
		iconUrl: 'carteAffichage/assets/' + getIconeSelonOption(option),
		iconSize: [25, 25],
		iconAnchor: [5, 15],
	});
	let tabMarkers = [];
	pointsIUT.forEach((point) => {
		let findIndex = tabIdIutHorsMap.indexOf(point.id_iut);
		if (findIndex === -1) {
			createPopup(map, layerGroupMap, point, iconPerso);
		} else {
			if (point.id_iut === idReunion) {
				createPopup(mapR, layerGroupMapReunion, point, iconPerso);
			} else if (point.id_iut === idGuyane) {
				createPopup(mapGuyane, layerGroupMapGuyane, point, iconPerso);
			} else {
				createPopup(mapIdf, layerGroupMapIleDeFrance, point, iconPerso);
			}
		}
	});
}

function createPopup(map, layerGroup, point, iconPerso) {
	var marker = L.marker([point.iut_latitude, point.iut_longitude], {
		icon: iconPerso,
	}).addTo(map);
	let popupContent = '<p class="iut-name">' + point.iut_name + '</br>';
	if (point.annee !== 'BUT 1') {
		popupContent +=
			'<span class="badge badge-pill badge-info">' +
			point.option_court +
			'</span> ';
		popupContent +=
			'<span class="badge badge-pill badge-warning">' +
			point.iut_mode_court +
			'</span><br>';
	}
	popupContent +=
		'<i class="mt-3 fa-solid fa-laptop"></i> <a href="' +
		point.iut_web_url +
		'" target="_blank">' +
		point.iut_web +
		'</a></p>';
	let popup = L.popup().setContent(popupContent);
	marker.bindPopup(popup);
	layerGroup.addLayer(marker);
}
