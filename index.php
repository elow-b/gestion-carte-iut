<?php
session_start();

include_once "router.php";
$router = new Router();

function isLogged()
{
  if (isset($_SESSION['log']) && $_SESSION['log'] === md5('carteR&T_gestion')) {
    return true;
  }
  return false;
}

$router->addRoute('GET', '/liste', function () {
  if (isLogged()) {
    echo file_get_contents('iut-tableau.html');
  } else {
    header('Location: ' . $_SERVER['SERVER_NAME']);
  }
  exit;
});

$router->addRoute('GET', '/blogs/:blogID', function ($blogID) {
  if (isLogged()) {
    echo file_get_contents('info/iut-carte.html');
  } else {
    header('Location: ' . $_SERVER['SERVER_NAME']);
  }
  exit;
});

$router->addRoute('GET', '/', function () {
  echo file_get_contents('connexion.html');
  exit;
});

$router->addRoute('GET', '/connexion', function () {
  $_SESSION['log'] = md5('carteR&T_gestion');
  header('Location: ' . $_SERVER['SERVER_NAME'] . '/liste');
  exit;
});

$router->addRoute('GET', '/blogs/:blogID', function ($blogID) {
  if (isLogged()) {
    echo file_get_contents('info/iut-carte.html');
  } else {
    header('Location: ' . $_SERVER['SERVER_NAME']);
  }
  exit;
});

$router->addRoute('GET', '/carte', function () {
  echo file_get_contents('carteAffichage/carte-BUT.html');
  exit;
});

$router->matchRoute();