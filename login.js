$(document).ready(function() {
  $('.alert').hide()
  $('#togglePassword').on('click', (e)=>{
    e.preventDefault()
    $('#iconPassword').toggleClass('bi-eye-slash-fill');
    if($('#password-rt').attr('type')==='password'){
      $('#password-rt').attr('type', 'text')
      $('#password-rt').attr('title', 'Afficher le mot de passe');
    }else{
      $('#password-rt').attr('type', 'password');
      $('#password-rt').attr('title', 'Masquer le mot de passe');
    }
  })
})

function submitForm(){
  let param = {
    mode:'connexion',
    login: '',
    mdp: ''
  }
  Object.values($('#loginForm input')).forEach(input=>{
    if(input.id === 'password-rt'){
      param.mdp = md5(input.value)
    }
    if(input.id === 'mail'){
      param.login = input.value 
    }
  })
  $.post('/ajaxCarte.php', param).then((retour)=>{
    if(JSON.parse(retour) === 'OK'){
      return window.location.replace('/connexion')
    }
      $('.erreur').text('Les identifiants sont incorrects.')
      $('.erreur').show()
      setTimeout(() => {
        $('.erreur').hide()
      }, 3000);
    
  })
}