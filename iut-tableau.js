

function getAllInfoIut(){
  if($('#configTableIUT').length >0){    
    $.get('ajaxCarte.php',{mode:'getAllIut'}).then((data)=>{
      $('#configTableIUT').bootstrapTable({data:JSON.parse(data),onClickRow:(row, element)=>{
        window.location.href = '/blogs/:' + row.id_iut;
      }})
    })
  }
}
getAllInfoIut()

function getInfo() {
  let formBut = document.getElementById('info');
  console.log('DATA', formBut)
  formBut.addEventListener('submit', (data)=>{
    data.preventDefault()
    let name = document.getElementById('iut')
    let adresse = document.getElementById('inputAddress')
    let codePostal = document.getElementById('inputZip')
    let ville = document.getElementById('inputCity')
    let lienComplet = document.getElementById('inputSite')
    let lienCourt = document.getElementById('inputSiteCourt')
    let param = {
      mode : 'saveModifs',
      nameIut: name.value,
      adresseIut:adresse.value,
      villeIut:ville.value,
      cpIut:codePostal.value,
      siteWeb:lienComplet.value,
      siteWebCourt:lienCourt.value
    }
    console.log(param)
    $.post('ajaxCarte.php',param,function(data){
       alert(JSON.parse(data))
    });
    
  })
}