<?php
include("db.php");

$data = file_get_contents('formations_22_23.json');

$tabData = json_decode($data, true);

error_log(print_r($tabData, true));
if (is_array($tabData)) {
  foreach ($tabData as $json) {
    $id = ''; 
    foreach ($json as $keyProperties => $properties) {
      error_log('REQ => NUM KEY ' .print_r($keyProperties, true));
      if ($keyProperties === 'properties'){
          $id = $properties['iD'];
          error_log('REQ id1 =>' . print_r($id, true));
          $req = 'INSERT INTO iut_info (id_iut, iut_name, iut_web, iut_web_url) VALUES (:id_iut, :iut_name, :iut_web, :iut_web_url)';
          //$req .= ' VALUES (' . $properties['iD'] . ',"' . $properties['nom'] .'","' . $properties['afficher_url']. '")';
          $db = $connexion->prepare($req);
          $db->bindParam('id_iut', $properties['iD'], PDO::PARAM_INT);
          $db->bindParam('iut_name', $properties['nom'], PDO::PARAM_STR_CHAR);
          $db->bindParam('iut_web', $properties['url_but'], PDO::PARAM_STR_CHAR);
          $db->bindParam('iut_web_url', $properties['url'], PDO::PARAM_STR_CHAR);
          //$db->execute();
          error_log('REQ =>' . print_r($req, true) . 'NUM KEY ' .print_r($keyProperties, true));
        } else if( $keyProperties ==='geometry'){
          $req = 'UPDATE iut_info SET iut_latitude = :iut_latitude, iut_longitude = :iut_longitude';
          $req .= ' WHERE id_iut = :id_iut ';
          $db = $connexion->prepare($req);
          $db->bindParam('id_iut', $id, PDO::PARAM_INT);
          $db->bindParam('iut_latitude', $properties['coordinates'][1], PDO::PARAM_STR_CHAR);
          $db->bindParam('iut_longitude', $properties['coordinates'][0], PDO::PARAM_STR_CHAR);
          error_log('REQ id2 =>' . print_r($id, true));
          //$db->execute();
        } else if( $keyProperties ==='iom'){
          if($properties['iom']){
            $iomId = 4;
            
            $req = 'INSERT INTO iut_formation_annee VALUES (0, :id_iut, :id_option, :id)';
            $db = $connexion->prepare($req);
            $db->bindParam('id_iut', $id, PDO::PARAM_INT);
            $db->bindParam('id_option', $iomId, PDO::PARAM_INT);
            error_log('REQ IOM =>' . print_r($properties, true));
            //$db->execute();
          }

        } else if( $keyProperties ==='cyber'){
          
        } else if( $keyProperties ==='pilpro'){
          
        } else if( $keyProperties ==='rom'){
          
        } else if( $keyProperties ==='devcloud'){
          
        } else if( $keyProperties ==='lp'){
          
        }
    }
  }
}

function insertOptionParAnnee($idIut, $idOption, $connexion){
  $tabAnnee = array(1,2,3);
 foreach($tabAnnee as $annee){

    $req = 'INSERT INTO iut_formation_annee VALUES (0, :id_iut, :id_annee, :id_option, :id_mode)';
    $db = $connexion->prepare($req);
    $db->bindParam('id_iut', $idIut, PDO::PARAM_INT);
    $db->bindParam('id_option', $idOption, PDO::PARAM_INT);
    //$db->execute();
 }
}

