<?php
use FFI\Exception;
include('db.php');
if (isset($_POST['mode'])) {
  $vars = $_POST;
} else {
  $vars = $_GET;
}
error_log('VARIABLE =>' . print_r($vars['mode'], true));
switch ($vars['mode']) {
  case 'connexion':
    if ($vars['login'] === 'communaute@neticien.net' && $vars['mdp'] === md5('carteR&T_gestion')) {
      $retour = 'OK';
    } else {
      $retour = 'KO';
    }
    break;
  case 'saveOptionBut':
    try {
      $delete = $connexion->prepare('DELETE FROM iut_formation_annee WHERE id_iut= ?');
      $req = $delete->execute(array($vars['idIut']));
      error_log('Delete réussi - index iut :' . print_r($vars['idIut'], true));
    } catch (Exception $e) {
      throw new Exception('Une erreur est survenue', 0);
    }
    foreach ($vars['option'] as $index => $option) {
      $optionDB = $connexion->query('SELECT * FROM but_option WHERE option_court ="' . $option . '"')->fetch(PDO::FETCH_ASSOC);
      $formationDB = $connexion->query('SELECT * FROM iut_mode_formation WHERE iut_mode_court ="' . $vars['modeFormation'][$index] . '"')->fetch(PDO::FETCH_ASSOC);
      $insert = $connexion->prepare("INSERT INTO iut_formation_annee (id_iut, iut_annee, iut_option, iut_mode) VALUES (?,?,?,?)");
      $retour = $insert->execute(array($vars['idIut'], $vars['annee'][$index], $optionDB['id_option'], $formationDB['id_mode']));
    }
    break;
  case 'getIut':
    if (strlen($vars['idIut']) === 0) {
      throw new Exception('Aucun iut selectionné', 0);
    }
    $select = $connexion->query("SELECT * FROM iut_info WHERE id_iut=" . $vars['idIut']);
    error_log('GETIUT' . print_r($select, true));
    $retour = $select->fetch(PDO::FETCH_ASSOC);
    if (count($retour) === 0) {
      throw new Exception('L\'IUT demandé n\'existe pas.', 0);
    }
    break;
  case 'getOptionButIut':
    if (strlen($vars['idIut']) === 0) {
      throw new Exception('Aucun iut selectionné', 0);
    }
    $req = 'SELECT ia.id_annee, bo.name_option, bo.option_court, imf.name_mode, imf.iut_mode_court';
    $req .= ' FROM iut_formation_annee ifa';
    $req .= ' INNER JOIN `iut-annee` ia ON ia.id_annee = ifa.iut_annee';
    $req .= ' INNER JOIN but_option bo ON bo.id_option = ifa.iut_option';
    $req .= ' INNER JOIN iut_mode_formation imf ON imf.id_mode = ifa.iut_mode';
    $req .= ' WHERE ifa.id_iut = ' . $vars['idIut'];
    $select = $connexion->query($req);
    $retour = $select->fetchAll(PDO::FETCH_ASSOC);
    break;
  case 'getAllIut':
    $select = $connexion->query('SELECT * FROM iut_info');
    $retour = $select->fetchAll(PDO::FETCH_ASSOC);
    break;
  case 'saveInfoIut':
    $id = $vars['idIut'];
    $reqUpdate = 'UPDATE iut_info SET ';
    $tabValue = [];
    $count = 0;
    foreach ($vars as $nomInfo => $info) {
      $count++;
      if ($nomInfo !== 'idIut' && $nomInfo !== 'mode' && strlen($info) > 0) {
        $reqUpdate .= strval($nomInfo) . '=?';
        if ($count !== count($vars)) {
          $reqUpdate .= ',';
        }
        $reqUpdate .= ' ';
        array_push($tabValue, $info);
      }
    }
    $reqUpdate .= ' WHERE id_iut=' . $id;
    $sql = $connexion->prepare($reqUpdate);
    $retour = $sql->execute($tabValue);
    break;
  default:
    $retour = [];
}
;
echo json_encode($retour);