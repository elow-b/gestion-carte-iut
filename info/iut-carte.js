const explodeUrl = window.location.href.split('/')
const iutId = explodeUrl[4].slice(1)
let iutInfo = {}

function getIut(){
  $('.alert').hide()
  let param = {
    mode:'getIut',
    idIut: iutId
  }
  $.get('/ajaxCarte.php',param).then((data)=>{
    console.log('passe la')
    iutInfo = JSON.parse(data)
    if(data.length > 0){
      $('input#iut_name').val(iutInfo.iut_name)
      $('input#iut_adresse').val(iutInfo.iut_adresse)
      $('input#iut_cp').val(iutInfo.iut_cp)
      $('input#iut_ville').val(iutInfo.iut_ville)
      $('input#iut_web').val(iutInfo.iut_web)
      $('input#iut_web_url').val(iutInfo.iut_web_url)
      initMap()
    }
  });
}

function getOptionBut(){
  let param = {
    mode:'getOptionButIut',
    idIut: iutId
  }
  $.get('/ajaxCarte.php',param).then((data)=>{
    let iutOptionInfo = JSON.parse(data)
    if(data.length > 0){
      console.log('option', iutOptionInfo)
      iutOptionInfo.forEach(annee => {
        if (annee.option_court === 'COMMUN') {
          let idBut1 = annee.iut_mode_court.toLowerCase() + '-' + annee.id_annee + '-' + annee.option_court.toLowerCase()
          $('#'+ idBut1).attr('checked', 'checked')
        }
        $('#switch-' + annee.option_court).attr('checked', 'checked');
        let id = annee.iut_mode_court.toLowerCase() + '-' + annee.id_annee + '-' + annee.option_court.toLowerCase();
        $('#'+ id).attr('checked', 'checked')
        $('#'+ id).removeAttr('disabled', 'disabled')
      });
    }

  });
}

function initMap(){
  let latLong = [46.603354, 1.8883335]
  if(parseInt(iutId) === 13){
    latLong = [5.1579834,-52.6424204]
  } else if(parseInt(iutId) === 25){
    latLong = [-21.1307379,55.5364801]
  }
  const map = L.map('map').setView(latLong, 5);
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  }).addTo(map);
  var marker = L.marker([iutInfo.iut_latitude, iutInfo.iut_longitude]).addTo(map);
  let popupContent = '<p class="iut-name">'+iutInfo.iut_name+'</p>'
  popupContent += '<p class="adresse">'+ iutInfo.iut_adresse+'<br>'+ iutInfo.iut_cp + ' ' + iutInfo.iut_ville+'</p>'
  popupContent += '<p>site web : <a class="site" href="'+iutInfo.iut_web_url+'" target="_blank">' + iutInfo.iut_web + '</a></p>'
  map.on('click', (e)=>{
    marker.bindPopup(popupContent).openPopup();
  })
}

$(document).ready(function() {
  $('#formInfoIut').on('submit', (e)=>{
    e.preventDefault()
  })
  $('#formBut').on('submit', (e)=>{
    e.preventDefault()
  })
  getIut()
  getOptionBut()

  $('.switch').on('change', function() {
    switch($(this).attr('id')){
      case 'switch-CYBER':
        afficheCaseAlternance('cyber', $(this))
        break;
      case 'switch-DEVCLOUD':
        afficheCaseAlternance('devcloud', $(this))
        break;
      case 'switch-IOM':
        afficheCaseAlternance('iom', $(this))
        break;
      case 'switch-PILPRO':
        afficheCaseAlternance('pilpro', $(this))
      case 'switch-ROM':
        afficheCaseAlternance('rom', $(this))
        break;
    }
  });
});

function saveInfoBut(){
  afficheSpinner('#btn-but') 
  let modeFormation=[]
  let annee=[]
  let option=[]
  
  Object.values($('#formBut input')).forEach(input=>{{
    if($(input).is(':checked')){
      let info = $(input).attr('id').split('-')
      if(info[0]!=='switch'){
        modeFormation.push(info[0]) 
        annee.push(info[1]) 
        option.push(info[2]) 
      }
    }
  }})
  let param = {
    mode:'saveOptionBut',
    idIut: iutId,
    option: option,
    annee: annee,
    modeFormation: modeFormation
  }
  $.post('/ajaxCarte.php', param).then((r)=>{
    console.log(r)
    afficheNotifSauvegardeOk('#btn-but')
    getOptionBut()
  }, ()=>{
    afficheNotifErreur()
  })
}

function afficheCaseAlternance(option, element){
  if ($(element).is(':checked')) {
    $('.alt-'+option).removeAttr('disabled');
  } else {
    $('.alt-'+option).attr('disabled', 'disabled');
    $('.alt-'+option).prop('checked', false)
  }
}
function afficheSpinner(idBtn){
  $(idBtn).attr('disable', 'disabled')
  $(idBtn).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>')
}
function saveIut(){
  afficheSpinner('#btn-iut')
  let param = {mode:'saveInfoIut', idIut:iutId}
  Object.values($('#formInfoIut input')).forEach(input=>{
    if(input.id && input.id.length > 0){
      if(input.id === 'inputZip' && input.value.length !== 5){
        console.log('erreur')
        return afficheNotifErreur("Le format du code postal est incorrect.")
      }
      param[input.id] = input.value;
    }
  })
  $.post('/ajaxCarte.php', param).then((r)=>{
    console.log(r)
    map.remove()
    $('#mapContainer').append('<div id="map"></div>')
    getIut()
    afficheNotifSauvegardeOk('#btn-iut')
  })
}

function afficheNotifErreur(message){
  if(!message){
    message = 'Une erreur est survenue';
  }
  $('.erreur').text(message)
  $('.erreur').show()

  setTimeout(() => {
    $('.erreur').alert('close')
  }, 3000);
}

function afficheNotifSauvegardeOk(idBtn){
  $('#save').show()
  console.log('save', $('#save'))
    setTimeout(() => {
      $('.save').alert('close')
      $(idBtn).removeAttr('disable')
      $(idBtn).html('Enregistrer')  
    }, 3000);
}